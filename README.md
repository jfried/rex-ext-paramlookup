# Rex::Ext::ParamLookup

A module to manage task parameters. Additionally it register the parameters as template values.

This module also looks inside a CMDB (if present) for a valid key.

# SYNTAX

```perl
my $var = param_lookup "param_name", "default_value";
```

# USAGE

```perl
use Rex::Ext::ParamLookup;

task setup => make {
  my $ntp_server = param_lookup "ntp_server", "ntp01.company.tld";
};
```
